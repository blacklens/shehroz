Rails.application.routes.draw do
  
=begin  match '*' => 'home#coming_soon', as: :coming_soon
=end

	mount Ckeditor::Engine => '/ckeditor'
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

root  'home#index'

post '/' => 'home#index'
get '/about-me' => 'about#show', as: :about_show
get '/blog' => 'blog#index', as: :blog_index
get '/consultation-success' => 'consultation_thanku#index', as: :cunsulation_index
get '/lean-program-success' => 'lean_program_thanku#index', as: :lean_index
get '/reverse-success' => 'reversetype_thanku#index', as: :reverse_index

get '/blog/:slug' => 'blog#show', as: :blog_show
get '/contact-us' => 'contact#show', as: :contact_show
# get '/gallery' => 'gallery#show', as: :gallery_show
get '/consultation' => 'consultation#index', as: :consultation_show
get '/coming-soon' => 'home#coming_soon', as: :coming_soon
get '/program/:slug' => 'programs#show', as: :program_show

get 'checkout' => 'checkout#index', as: :checkout_index
get 'program-checkout' => 'program_checkout#index', as: :program_checkout_index
get 'client-assesment' => 'client_assesment#index', as: :client_assesment_index

post '/bookings' => 'bookings#create', as: :create_bookings
post '/temp_bookings' => 'temp_bookings#create', ad: :temp_bookings

   	get '/mail-design' => 'mail_designs#new'
   	post '/mail-design' => 'mail_designs#create'
   	post '/enquiry' => 'enquiry#create'

   	get '/marketing-emails' => 'emailer#show'
	post '/marketing-emails' => 'emailer#create', as: :marketing_emails_create
    get '/emailer/template' => 'emailer#template'
	 post '/contact-us' => 'contact#submit'
get '/client-assessment' => 'feedback#show', as: :feedback_show

end