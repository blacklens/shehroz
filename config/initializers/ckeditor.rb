require "ckeditor/orm/active_record"
Ckeditor.setup do |config|
  # //cdn.ckeditor.com/<version.number>/<distribution>/ckeditor.js
  config.parent_controller = 'EmailerController'
  assets_root =  Rails.root.join('app','assets','javascripts')
  ckeditor_plugins_root = assets_root.join('ckeditor','plugins')
  	%w(openlink sourcedialog).each do |ckeditor_plugin|
    	Ckeditor.assets += Dir[ckeditor_plugins_root.join(ckeditor_plugin, '**', '*.js')].map {|x| x.sub(assets_root.to_path, '').sub(/^\/+/, '')}
	end
end
