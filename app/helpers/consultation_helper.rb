module ConsultationHelper

	def not_booked(date, time)
		return !Booking.find_by(booking_date: date.to_date, booking_time: time).present?
	end
end
