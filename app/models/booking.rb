class Booking < ApplicationRecord
	before_create :create_uid
	after_create :send_mails

	def create_uid
	    self.uid = loop do
	      	random_uid = "SHRO#{rand.to_s[2..11]}"
	      	break random_uid unless Booking.exists?(uid: random_uid)
	   	end
	end

	def self.get_boking_type(amount)
		if amount.to_i <= 1000
			return 'slot_booking'
		else
			return 'program_booking'

		end
	end

	def send_mails
		if self.status == 'captured'
			# if self.amount == 200
				BookingMailer.admin_mail(self).deliver_now
				BookingMailer.customer_mail(self).deliver_now
			# else
			# 	ProgramMailer.admin_mail(self).deliver_now
			# 	ProgramMailer.customer_mail(self).deliver_now
			# end	
			
		end
	end
	rails_admin do
			show do
		      field :uid
		      field :status
		      field :amount
		      field :currency
		      field :email
		      field :phone
		     
		  
	    	end
	    	edit do
		      field :uid
		      field :status
		      field :amount
		      field :currency
		      field :email
		      field :phone
		     
	    	end
		    list do
		      field :uid
		      field :email
		      field :phone
		      field :status
		      field :amount
		      field :currency
		     
		    end
		end

end
