class Blog < ApplicationRecord
extend FriendlyId
  	friendly_id :heading, use: :slugged
	mount_uploader :img, AvatarUploader

rails_admin do

	    	edit do

		      field :heading
		      field :desc
		      field :img
		      field :video_link
		     field :symmery_one, :ck_editor do
	      		config_js ActionController::Base.helpers.asset_path('ckeditor/config.js')
	    	end
	    end
		    
		end
end
