# frozen_string_literal: true

class Ckeditor::Picture < Ckeditor::Asset
	def self.inheritance_column
    	nil
  	end
  	mount_uploader :data, CkeditorPictureUploader, mount_on: :data_file_name

  def url_content
    "https://shehrozalam.com#{url(:content)}"
  end

end
