class TempBooking < ApplicationRecord
	validates_inclusion_of :booking_type, :in => ['slot_booking', 'program_booking']
	validates_inclusion_of :booking_time, :in => ["1:00 PM","1:30 PM","2:00 PM","2:30 PM","3:00 PM","3:30 PM","4:00 PM","4:30 PM","5:00 PM","5:30 PM","6:00 PM"], if: :slot_booking
	before_save :validate_date, if: :slot_booking
	validates_presence_of :email
	validates_presence_of :phone
	validates_presence_of :booking_date
	validates_presence_of :booking_time
	validates_presence_of :booking_type
	validates_presence_of :amount

		
	def validate_date
		if (self.booking_date.to_date < Date.today) || (self.booking_date.to_date == Date.today && self.booking_time.to_time < Time.now)
			self.errors.add(:booking_date, "Invalid Date")
			throw(:abort)
		else
			self.booking_date = self.booking_date.to_date
		end
	end
	def slot_booking
	    self.booking_type == 'slot booking'
	end
end
