class Program < ApplicationRecord
	extend FriendlyId
  	friendly_id :heading, use: :slugged
	mount_uploader :img, AvatarUploader
	# has_rich_text :content_two
		rails_admin do

	 edit do
		 	field :heading
		 	field :short_desc
		 	field :paragraph_one
		 	field :paragraph_two
		 	field :amount
		 	field :amount
		 	field :payment_link
		 	field :actual_price
		 	field :discount
	 		field :slug
	  		field :img
	    	field :video_link
	      	field :content, :ck_editor do
	      		config_js ActionController::Base.helpers.asset_path('ckeditor/config.js')
end
	    end
end     

end
