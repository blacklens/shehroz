class AboutMe < ApplicationRecord
		mount_uploader :img, AvatarUploader
		mount_uploader :bg, AvatarUploader

	rails_admin do

		 edit do
		 	include_all_fields
	      	field :certificate_points, :ck_editor do
	      		config_js ActionController::Base.helpers.asset_path('ckeditor/config.js')
	      	end
		end
    end

end
