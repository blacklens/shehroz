class SlotBooking < Booking
	enum timings: {"1:00 PM" => '1:00 PM', "2:00 PM" => "2:00 PM", "3:00 PM" => "3:00 PM", "4:00 PM" => "4:00 PM", "5:00 PM" => "5:00 PM", "6:00 PM" => "6:00 PM"}

	rails_admin do
		show do
	      field :uid
	      field :status
	      field :amount
	      field :currency
	      field :email
	      field :phone
	      field :type
	      field :booking_date
	      field :booking_time
	  
    	end
    	edit do
	      field :uid
	      field :status
	      field :amount
	      field :currency
	      field :email
	      field :phone
	      field :type
	      field :booking_date
	      field :booking_time
    	end
	    list do
	      field :uid
	      field :email
	      field :phone
	      field :booking_date
	      field :booking_time
	      field :status
	      field :amount
	      field :currency
	      field :type
	     
	    end
	end
end
