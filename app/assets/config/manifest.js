// JS and CSS bundles
//
//= link application.js
//= link application.css

// Images and fonts so that views can link to them
//
//= link_tree ../fonts
//= link_tree ../images

// Custom
//
// CKEditor gem assets
//= link ckeditor/config.js
//= link ckeditor/application.css
//= link ckeditor/application.js
//