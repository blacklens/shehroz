$(document).ready(function() {
  
  
  $('#my-carousel').on('initialized.owl.carousel', function() {
$('.navigator').eq(0).addClass('active');
console.log('initialized');
});
  
$('#my-carousel').owlCarousel({
loop : true,
autoplay : true,
autoplayTimeout : 5000,
nav : false,
dots : true,
singleItem: true,
responsive:{
0:{
items:1
},
600:{
items:1
},
1000:{
items:1
}
}
});

$('#my-carousel').on('changed.owl.carousel', function(ev) {
var item_index = ev.page.index;
$('.navigator').removeClass('active').eq(item_index).addClass('active');
});

$('.navigator').on('click', function() {
var item_no = $(this).data('item'); 
$('#my-carousel').trigger('to.owl.carousel', [item_no, 3000, true]);
});
});

