$(document).ready(function(){

  $("#checkout-form").validate({
    wrapper: 'div',
    rules: {
      "temp_booking[email]": {required: true, email: true, emailcomplete: true},
      "temp_booking[name]": {required: true,lettersonlys: true, noSpace: false},
      "temp_booking[phone]": {required: true, noSpace: true, maxlength: 10, minlength: 10, number: true}
    },
    messages: {
      "temp_booking[email]": {required: "*Email is required",emailcomplete: "*Complete Email Address",email: "*Valid email address or remove space from the end of the email."},
      "temp_booking[name]": {required: "*Name is required", lettersonlys: "*Enter valid name", noSpace: "*Enter valid name"},
      "temp_booking[phone]": {required: "*Phone number is required", noSpace: "*Enter valid number",number: "*Enter valid number"}
    }
  });

  jQuery.validator.addMethod("noSpace", function(value, element) {
    return this.optional(element) || /^[^\s]+(\s+[^\s]+)*$/.test(value);
  });
  jQuery.validator.addMethod("lettersonlys", function(value, element) {
    return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
  }, "*Enter alphabets only.");
  jQuery.validator.addMethod("emailcomplete", function(value, element) {
    return this.optional(element) ||  /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(value);
  });
})
