class BookingsController < ApplicationController
	skip_before_action :verify_authenticity_token
	before_action :set_params
	before_action :find_temp_booking
	def create
		if Booking.find_by(pay_id: payment_params[:pay_id]).blank?
			booking = Booking.new(payment_params.merge!(user_params))
			if @temp_booking.present?
				booking.booking_date = @temp_booking.booking_date
				booking.type = @temp_booking.booking_type.classify
				booking.booking_time = @temp_booking.booking_time
				booking.name = @temp_booking.name
			end
			if booking.save
				@temp_booking.update_attributes(is_booked: true) if @temp_booking.present? && booking.status == 'captured'
			else
				#failure mail to developer urgent to check
			end
		end
	end

	private

	def payment_params
		params.require(:payload).require(:payment).require(:entity).permit(:status, :pay_id, :amount, :currency, :pay_date)
	end

	def user_params
		params.require(:payload).require(:payment).require(:entity).require(:notes).permit(:email, :name, :phone)
	end

	def set_params
		params[:payload][:payment][:entity][:pay_id] = params[:payload][:payment][:entity][:id]
		params[:payload][:payment][:entity][:pay_date] = params[:payload][:payment][:entity][:created_at]
		params[:payload][:payment][:entity][:amount] = (params[:payload][:payment][:entity][:amount]/100).to_f
	end

	def find_temp_booking
		booking_type = Booking.get_boking_type(payment_params[:amount])
		@temp_booking = TempBooking.where("(email ILIKE ? or phone = ?) AND booking_type = ? AND is_booked = false", "#{user_params[:email].split('@').first}@%", user_params[:phone], booking_type).order("created_at DESC").first
		# @temp_booking = TempBooking.where(email: user_params[:email], phone: user_params[:phone], booking_type: booking_type, is_booked: false).order("created_at DESC").first
	end
end
