class ConsultationController < ApplicationController

	def index
		@about = AboutMe.first
		@consult = Consultation.first

		@current_time = Time.now.in_time_zone(TZInfo::Timezone.get('Asia/Kolkata')).asctime.in_time_zone("UTC")
		if params[:date].blank? || (params[:date].present? && (params[:date].to_date < Date.today))
			if SlotBooking.timings.values.last.to_time < @current_time
				@date = Date.today + 1.day
			else
				@date = Date.today+ 1.day
			end
		else 
			@date = params[:date].to_date
		end
		#@days_of_week = (@date.at_beginning_of_week..@date.at_end_of_week).to_a
		@days_of_week = (@date..(@date+4.days)).to_a
	end
end
