class EmailerController < ApplicationController
	layout :false
	STATIC_MODELS = ["contacts", "slot_booking", "temp_booking", "program_booking"]

	def create
 		if params[:ip] == "192.168.0.1" && params[:password] == "Changeit"
 			if params[:email_model].downcase == "all"
 				STATIC_MODELS.each do |email_model|
 					send_mail(email_model)
 				end
 			else
				send_mail(params[:email_model])
 			end
			redirect_to marketing_emails_create_path
		else
			BookingMailer.report_mail(params).deliver_now
			flash[:notice] = 'Oops! Auth went wrong.'
			redirect_to root_path
		end
	end
	def index
		@templates = MailTemplate.all
	end
	def show
		@templates = MailTemplate.all
	end

	def template
		@template = MailTemplate.find_by(id: params[:template_id])
	end

	private

	def send_mail email_model
		model_name = email_model.gsub(' ','_').classify.constantize rescue nil
		if model_name.present?
			uniq_model_records = model_name.all.uniq{|x| x.email}
			uniq_model_records.each do |user|
				BookingMailer.bulk_mail(user.email, user.name || user.email.split('@').first, params[:template_body],params[:subject]).deliver_now
			end
			BookingMailer.report_mail(params).deliver_now
		else
			flash[:nocice] = 'Invalid email list'
		end
	end

end
