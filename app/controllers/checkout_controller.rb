class CheckoutController < ApplicationController
	before_action :check_booking_slot

	def index
		@temp_booking = TempBooking.new(booking_date: params[:date].to_date, booking_time: params[:time], amount: params[:amount], booking_type: params[:booking_type])
		@consult = Consultation.first
	end

	private

	def check_booking_slot
		if params[:booking_type] == 'slot_booking' && (params[:date].present? && params[:time].present?)
			booking = SlotBooking.find_by(booking_date: params[:date].to_date, booking_time: params[:time])
			if booking.present?
				flash[:notice] = "This booking date is not available. Please try to book for other slots."
				redirect_to consultation_show_path
			end
		else
			flash[:notice] == 'Invalid booking request.'
			redirect_to consultation_show_path
		end
	end
end
