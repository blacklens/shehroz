class MailDesignsController < ApplicationController
	layout :false

	def new
	end

	def create
		if params[:ip] == "192.168.0.1" && params[:password] == "Changeit"
			mail_design = MailDesign.new(mail_design_params)
			if mail_design.save
				
				flash[:success] = "Thanks! Please check your inbox."
			else
				flash[:notice] = "Error!"
			end
			redirect_to '/mail-design'
		else
		# 	BookingMailer.report_mail(params).deliver_now
		# 	flash[:notice] = 'Oops! Auth went wrong.'
		# 	redirect_to root_path
		end
	end

	private

	def mail_design_params
		params.permit(:subject, :body, :attachment)
	end
end
