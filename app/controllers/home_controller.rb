class HomeController < ApplicationController
	def index
		@sliders = Slider.all
		@points = SixPoint.all
		@intros = ShortIntro.all
		@testimonials = TestimonialClient.all
		@inputheader = InputHeader.first
		@inputtext = InputText.first
	end
	def submit
		contact = Contact.new(contact_params)
		if contact.save
		flash[:success] = 'Thanks, Your contact request is generated. We will contact you soon!'
		# ContactMailer.send_contact_mail(params).deliver_now
		# ContactMailer.customer_mail(params).deliver_now
		else
			flash[:notice] = 'Oops! Something went wrong.'
		end
		redirect_to root_path
	end

	private
	def contact_params
		params.permit(:name, :email, "", "")
	end
end
