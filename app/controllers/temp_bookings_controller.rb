class TempBookingsController < ApplicationController
	before_action :find_temp_booking
	def create
		if @temp_booking.present?
			if @temp_booking.update(booking_params)

				@success = true
			else
				flash[:notice] = "Something went wrong. Please try again."
			end
		else
			flash[:notice] = "Something went wrong. Please try again."
		end
	end

	private

	def find_temp_booking
		@temp_booking = TempBooking.find_or_initialize_by(email: booking_params[:email], phone: booking_params[:phone], booking_type: booking_params[:booking_type], booking_date: booking_params[:booking_date], booking_time: booking_params[:booking_time])
	end

	def booking_params
		params.require(:temp_booking).permit(:name, :email, :phone, :booking_type, :amount, :booking_date, :booking_time)
	end
end
