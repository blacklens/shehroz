class ApplicationController < ActionController::Base
	before_action :load_program

	private
	def load_program
		@programs = Program.all
		@program = Program.friendly.find_by(slug: params[:slug])
	end
end
