class BlogController < ApplicationController
	def index
		@about = AboutMe.first
		@blogs = Blog.all
	end
	def show
		@about = AboutMe.first
		@blogs = Blog.all
		@blog = Blog.friendly.find_by(slug: params[:slug])
		@programs = Program.all
		@program = Program.friendly.find_by(slug: params[:slug])
	end
	
end
