class ProgramCheckoutController < ApplicationController
	# before_action :find_temp_booking

	def index
		@temp_booking = TempBooking.new(payment_link: params[:pay_link],booking_date: Date.today.to_date.strftime("%Y-%m-%d"), booking_time: Time.now.to_time.strftime("%l:%M %p").strip, heading: params[:program], desc: params[:desc] , amount: params[:amount], booking_type: params[:booking_type])
	end

end
