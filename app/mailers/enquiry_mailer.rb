class EnquiryMailer < ApplicationMailer
	def enquiry_mail(mail_design_id, email)
		@mail_design = MailDesign.find_by(id: mail_design_id)
		if @mail_design.attachment.present?
	    	attachments["attachment.pdf"] = File.read("#{Rails.root}/public/#{@mail_design.attachment_url}")
	    end
		mail( :to => email,:subject => @mail_design.subject )
	end
end
