class ApplicationMailer < ActionMailer::Base
 default from: "Shehroz Alam <info@shehrozalam.com>"
  layout 'mailer'
end
