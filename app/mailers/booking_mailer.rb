class BookingMailer < ApplicationMailer

	def admin_mail(booking)
		@booking = booking
		mail( :to => ["shehrozalambhat@gmail.com"], :bcc => ["sharmalancer@gmail.com"], :subject => "New Booking for Consultation" )
	end
	def customer_mail(booking)
		@booking = booking
		mail( :to =>  @booking.email,:subject => "Thanks! Your Booking is Confirmed" )
	end
	def bulk_mail(email, name, body,subject)
		@name = name
		@body = body
		@subject = subject
		mail( :to => email,:subject => subject )
	end
	def report_mail(params)
		@params = params
		mail( :to => ["shehrozalambhat@gmail.com"], :bcc => ["sharmalancer@gmail.com"], :subject => "Alert! Bulk mail Report." )
	end
end
