class ProgramMailer < ApplicationMailer

	def admin_mail(booking)
		@booking = booking
		mail( :to => ["shehrozalambhat@gmail.com"], :bcc => ["sharmalancer@gmail.com"], :subject => "New Program Booking from client" )
	end
	def customer_mail(booking)
		@booking = booking
		mail( :to =>  @booking.email,:subject => "Thanks! Your Program Booking." )
	end
end