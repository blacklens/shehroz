# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_09_27_171800) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "about_mes", force: :cascade do |t|
    t.string "main_heading"
    t.text "main_desc"
    t.string "video"
    t.text "long_desc"
    t.text "certification_desc"
    t.string "img"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "bg"
    t.string "vision_heading"
    t.text "vision_desc"
  end

  create_table "action_text_rich_texts", force: :cascade do |t|
    t.string "name", null: false
    t.text "body"
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["record_type", "record_id", "name"], name: "index_action_text_rich_texts_uniqueness", unique: true
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "blogs", force: :cascade do |t|
    t.string "heading"
    t.string "desc"
    t.string "img"
    t.string "video_link"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.text "symmery_one"
    t.text "heading_one"
    t.text "symmery_two"
    t.text "quote"
    t.text "heading_two"
    t.text "symmery_three"
    t.string "slug"
    t.index ["slug"], name: "index_blogs_on_slug", unique: true
  end

  create_table "bookings", force: :cascade do |t|
    t.string "uid"
    t.string "status"
    t.string "pay_id"
    t.float "amount"
    t.string "currency"
    t.string "name"
    t.string "email"
    t.string "phone"
    t.datetime "pay_date"
    t.string "type"
    t.date "booking_date"
    t.string "booking_time"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string "data_file_name", null: false
    t.string "data_content_type"
    t.integer "data_file_size"
    t.string "type", limit: 30
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["type"], name: "index_ckeditor_assets_on_type"
  end

  create_table "consultations", force: :cascade do |t|
    t.text "heading"
    t.text "desc"
    t.text "sub_desc"
    t.text "chckout_desc"
    t.string "amount"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "payment_link"
  end

  create_table "contacts", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.text "message"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "faq_data", force: :cascade do |t|
    t.string "heading"
    t.text "desc"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "feedbacks", force: :cascade do |t|
    t.string "heading"
    t.text "desc"
    t.string "img"
    t.string "video_link"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string "slug", null: false
    t.integer "sluggable_id", null: false
    t.string "sluggable_type", limit: 50
    t.string "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
    t.index ["sluggable_type", "sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_type_and_sluggable_id"
  end

  create_table "input_headers", force: :cascade do |t|
    t.string "heading"
    t.string "desc"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "input_texts", force: :cascade do |t|
    t.string "heading"
    t.string "desc"
    t.string "name_value"
    t.string "email_value"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "mail_designs", force: :cascade do |t|
    t.text "body"
    t.string "attachment"
    t.string "subject"
    t.boolean "is_active", default: true
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "mail_templates", force: :cascade do |t|
    t.text "body"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "programs", force: :cascade do |t|
    t.string "heading"
    t.text "short_desc"
    t.string "content"
    t.string "video_link"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "img"
    t.string "slug"
    t.string "amount"
    t.string "payment_link"
    t.string "content_one"
    t.string "content_two"
    t.string "actual_price"
    t.string "discount"
    t.text "paragraph_one"
    t.text "paragraph_two"
    t.index ["slug"], name: "index_programs_on_slug", unique: true
  end

  create_table "purchased_programs", force: :cascade do |t|
    t.string "programe_name"
    t.string "customer_name"
    t.string "mobile"
    t.string "email"
    t.string "status"
    t.string "amount"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "short_intros", force: :cascade do |t|
    t.string "img"
    t.string "heading"
    t.string "description"
    t.string "short_desc"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "six_points", force: :cascade do |t|
    t.string "icon"
    t.string "heading"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "sliders", force: :cascade do |t|
    t.string "img"
    t.string "heading"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "link"
  end

  create_table "temp_bookings", force: :cascade do |t|
    t.string "email"
    t.string "phone"
    t.string "name"
    t.float "amount"
    t.date "booking_date"
    t.string "booking_time"
    t.string "booking_type"
    t.boolean "is_booked", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "heading"
    t.string "desc"
    t.string "payment_link"
  end

  create_table "testimonial_clients", force: :cascade do |t|
    t.string "video_link"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
end
