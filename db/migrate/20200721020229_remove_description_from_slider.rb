class RemoveDescriptionFromSlider < ActiveRecord::Migration[6.0]
  def change
    remove_column :sliders, :description, :string
  end
end
