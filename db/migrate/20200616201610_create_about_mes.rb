class CreateAboutMes < ActiveRecord::Migration[6.0]
  def change
    create_table :about_mes do |t|
      t.string :main_heading
      t.text :main_desc
      t.string :video
      t.text :long_desc
      t.text :certification_desc
      t.string :img

      t.timestamps
    end
  end
end
