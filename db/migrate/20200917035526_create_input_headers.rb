class CreateInputHeaders < ActiveRecord::Migration[6.0]
  def change
    create_table :input_headers do |t|
      t.string :heading
      t.string :desc
      t.string :name

      t.timestamps
    end
  end
end
