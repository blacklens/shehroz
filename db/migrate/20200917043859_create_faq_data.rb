class CreateFaqData < ActiveRecord::Migration[6.0]
  def change
    create_table :faq_data do |t|
      t.string :heading
      t.text :desc

      t.timestamps
    end
  end
end
