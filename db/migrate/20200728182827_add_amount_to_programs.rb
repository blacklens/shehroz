class AddAmountToPrograms < ActiveRecord::Migration[6.0]
  def change
    add_column :programs, :amount, :string
    add_column :programs, :payment_link, :string

  end
end
