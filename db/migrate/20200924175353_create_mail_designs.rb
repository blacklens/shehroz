class CreateMailDesigns < ActiveRecord::Migration[6.0]
  def change
    create_table :mail_designs do |t|
    	t.text :body
    	t.string :attachment
    	t.string :subject
    	t.boolean :is_active, default: true
      t.timestamps
    end
  end
end
