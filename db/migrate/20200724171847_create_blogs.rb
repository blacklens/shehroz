class CreateBlogs < ActiveRecord::Migration[6.0]
  def change
    create_table :blogs do |t|
      t.string :heading
      t.string :desc
      t.string :img
      t.string :video_link

      t.timestamps
    end
  end
end
