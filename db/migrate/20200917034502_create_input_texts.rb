class CreateInputTexts < ActiveRecord::Migration[6.0]
  def change
    create_table :input_texts do |t|
      t.string :heading
      t.string :desc
      t.string :name_value
      t.string :email_value

      t.timestamps
    end
  end
end
