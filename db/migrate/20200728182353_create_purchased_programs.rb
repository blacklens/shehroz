class CreatePurchasedPrograms < ActiveRecord::Migration[6.0]
  def change
    create_table :purchased_programs do |t|
      t.string :programe_name
      t.string :customer_name
      t.string :mobile
      t.string :email
      t.string :status
      t.string :amount

      t.timestamps
    end
  end
end
