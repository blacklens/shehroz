class AddLinkToSlider < ActiveRecord::Migration[6.0]
  def change
    add_column :sliders, :link, :string
  end
end
