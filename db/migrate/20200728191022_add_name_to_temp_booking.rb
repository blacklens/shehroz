class AddNameToTempBooking < ActiveRecord::Migration[6.0]
  def change
    add_column :temp_bookings, :heading, :string
    add_column :temp_bookings, :desc, :string
  end
end
