class CreateShortIntros < ActiveRecord::Migration[6.0]
  def change
    create_table :short_intros do |t|
      t.string :img
      t.string :heading
      t.string :description
      t.string :short_desc

      t.timestamps
    end
  end
end
