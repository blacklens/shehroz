class CreateTempBookings < ActiveRecord::Migration[6.0]
  def change
    create_table :temp_bookings do |t|
    	t.string :email
    	t.string :phone
      t.string :name
    	t.float :amount
    	t.date :booking_date
    	t.string :booking_time
      t.string :booking_type
      t.boolean :is_booked, default: false
      t.timestamps
    end
  end
end
