class CreateSliders < ActiveRecord::Migration[6.0]
  def change
    create_table :sliders do |t|
      t.string :img
      t.string :heading
      t.string :description

      t.timestamps
    end
  end
end
