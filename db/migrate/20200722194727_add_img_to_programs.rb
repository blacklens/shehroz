class AddImgToPrograms < ActiveRecord::Migration[6.0]
  def change
    add_column :programs, :img, :string
  end
end
