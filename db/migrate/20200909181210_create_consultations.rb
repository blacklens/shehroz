class CreateConsultations < ActiveRecord::Migration[6.0]
  def change
    create_table :consultations do |t|
      t.text :heading
      t.text :desc
      t.text :sub_desc
      t.text :chckout_desc
      t.string :amount

      t.timestamps
    end
  end
end
