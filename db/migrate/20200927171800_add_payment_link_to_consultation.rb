class AddPaymentLinkToConsultation < ActiveRecord::Migration[6.0]
  def change
    add_column :consultations, :payment_link, :string
  end
end
