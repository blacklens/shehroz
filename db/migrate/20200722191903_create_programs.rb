class CreatePrograms < ActiveRecord::Migration[6.0]
  def change
    create_table :programs do |t|
      t.string :heading
      t.text :short_desc
      t.string :content
      t.string :video_link

      t.timestamps
    end
  end
end
