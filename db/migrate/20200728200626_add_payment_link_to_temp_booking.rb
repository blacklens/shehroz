class AddPaymentLinkToTempBooking < ActiveRecord::Migration[6.0]
  def change
    add_column :temp_bookings, :payment_link, :string
  end
end
