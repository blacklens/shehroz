class CreateBookings < ActiveRecord::Migration[6.0]
  def change
    create_table :bookings do |t|
    	t.string :uid
    	t.string :status
    	t.string :pay_id
    	t.float :amount
    	t.string :currency
    	t.string :name
    	t.string :email
    	t.string :phone
    	t.datetime :pay_date
      t.string :type
      t.date :booking_date
      t.string :booking_time
      t.timestamps
    end
  end
end