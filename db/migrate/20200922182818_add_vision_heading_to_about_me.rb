class AddVisionHeadingToAboutMe < ActiveRecord::Migration[6.0]
  def change
    add_column :about_mes, :vision_heading, :string
    add_column :about_mes, :vision_desc, :text

  end
end
