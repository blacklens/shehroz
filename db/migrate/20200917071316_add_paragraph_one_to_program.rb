class AddParagraphOneToProgram < ActiveRecord::Migration[6.0]
  def change
    add_column :programs, :paragraph_one, :text
    add_column :programs, :paragraph_two, :text

  end
end
