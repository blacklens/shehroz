class CreateTestimonialClients < ActiveRecord::Migration[6.0]
  def change
    create_table :testimonial_clients do |t|
      t.string :video_link

      t.timestamps
    end
  end
end
