class CreateSixPoints < ActiveRecord::Migration[6.0]
  def change
    create_table :six_points do |t|
      t.string :icon
      t.string :heading
      t.text :description

      t.timestamps
    end
  end
end
