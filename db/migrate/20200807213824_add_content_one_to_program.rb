class AddContentOneToProgram < ActiveRecord::Migration[6.0]
  def change
    add_column :programs, :content_one, :string
    add_column :programs, :content_two, :string
	add_column :programs, :actual_price, :string
	add_column :programs, :discount, :string
  end
end
