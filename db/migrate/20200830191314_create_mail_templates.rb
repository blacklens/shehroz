class CreateMailTemplates < ActiveRecord::Migration[6.0]
  def change
    create_table :mail_templates do |t|
      t.text :body
      t.string :name

      t.timestamps
    end
  end
end
